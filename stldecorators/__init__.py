import os
import sys

# Determine if folder is in sys.path already
fldr = os.path.dirname(__file__)
in_path = fldr in sys.path

# If folder not in sys.path, add it to allow import of modules
if not in_path:
    sys.path.append(fldr)

# Import custom modules
import stl_logging as logging  # noqa:E402
import stl_files as files  # noqa:E402

# Remove folder from sys.path if it wasn't there originally
if not in_path:
    if sys.path.pop() != fldr:
        raise ImportError("There was an error in removing the import folder from the pythonpath")
