""" Decorator functions relating to files based on the output of a function

"""


# Imports
import functools
import logging
import errno
import os
from typing import Callable, TypeVar, cast, Any


F = TypeVar('F', bound=Callable[..., Any])


def exists(fnc: F) -> F:
    "Throw a FileNotFoundError if path returned does not exist"
    @functools.wraps(fnc)
    def wrap(*args, **kwargs) -> Any:
        out = fnc(*args, **kwargs)
        logging.debug("Confirming that path exists - {f}".format(f=out))
        if not os.path.exists(out):
            logging.error("Path does not exist - {f}".format(f=out))
            raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), out)
        else:
            return out
    return cast(F, wrap)


def is_file(fnc: F) -> F:
    """ Throw a IsADirectoryError if path returned is not a file

    Uses exists function to confirm path exists
    """
    @functools.wraps(fnc)
    def wrap(*args, **kwargs) -> Any:
        out = exists(fnc)(*args, **kwargs)
        logging.debug("Confirming that path is a file - {f}".format(f=out))
        if not os.path.isfile(out):
            logging.error("Path is not a file - {f}".format(f=out))
            raise IsADirectoryError(errno.EISDIR, os.strerror(errno.EISDIR), out)
        else:
            return out
    return cast(F, wrap)


def is_dir(fnc: F) -> F:
    """ Throw a NotADirectoryError if path returned is not a directory

    Uses exists function to confirm path exists
    """
    @functools.wraps(fnc)
    def wrap(*args, **kwargs) -> Any:
        out = exists(fnc)(*args, **kwargs)
        logging.debug("Confirming that path is a directory - {f}".format(f=out))
        if not os.path.isdir(out):
            logging.error("Path is not a directory - {f}".format(f=out))
            raise NotADirectoryError(errno.ENOTDIR, os.strerror(errno.ENOTDIR), out)
        else:
            return out
    return cast(F, wrap)


def make_dirs(fnc: F) -> F:
    "Create required folders if dirname of path returned doesn't exist"
    @functools.wraps(fnc)
    def wrap(*args, **kwargs) -> Any:
        out = fnc(*args, **kwargs)
        out_dir = os.path.dirname(out)
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        return out
    return cast(F, wrap)
