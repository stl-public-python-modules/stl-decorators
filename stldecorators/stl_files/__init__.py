import os
import sys

# Determine if folder is in sys.path already
fldr = os.path.dirname(__file__)
in_path = fldr in sys.path

# Add folder to sys.path if required to allow import of modules
if not in_path:
    sys.path.append(fldr)

# Import custom modules
import stl_file_input as input  # noqa:E402
import stl_file_output as output  # noqa:E402

# Remove folder from sys.path if it wasn't there originally
if not in_path:
    if sys.path.pop() != fldr:
        raise ImportError("There was an error in removing the import folder from the pythonpath")
