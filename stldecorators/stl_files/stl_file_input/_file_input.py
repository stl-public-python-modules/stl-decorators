""" Decorator functions relating to files based on the input of a function

"""


# Imports
import functools
import logging
import errno
import os
from typing import Callable, TypeVar, cast, Any, Optional


F = TypeVar('F', bound=Callable[..., Any])


def exists(_fnc: Optional[F] = None, *, var_name: str, pos: int = 0) -> Any:
    """ Throw a FileNotFoundError if path given as an argument does not exist

    :param var_name: Name of argument containing the path
    :param pos: Position in argument list of argument containing path (0 index)
    """
    def dec(fnc: F) -> F:
        @functools.wraps(fnc)
        def wrap(*args, **kwargs) -> Any:
            loc = kwargs[var_name] if var_name in kwargs else args[pos]
            logging.debug("Confirming that path exists - {f}".format(f=loc))
            if not os.path.exists(loc):
                logging.error("Path does not exist - {f}".format(f=loc))
                raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), loc)
            else:
                return fnc(*args, **kwargs)
        return cast(F, wrap)
    if _fnc is None:
        return dec
    else:
        return dec(_fnc)


def is_file(_fnc: Optional[F] = None, *, var_name: str, pos: int = 0) -> Any:
    """ Throw a IsADirectoryError if location given as an argument is not a file

    Uses exists function to first confirm that path exists
    :param var_name: Name of argument containing the path
    :param pos: Position in argument list of argument containing path (0 index)
    """
    def dec(fnc: F) -> F:
        @functools.wraps(fnc)
        @exists(var_name=var_name, pos=pos)
        def wrap(*args, **kwargs) -> Any:
            loc = kwargs[var_name] if var_name in kwargs else args[pos]
            logging.debug("Confirming that path is a file - {f}".format(f=loc))
            if not os.path.isfile(loc):
                logging.error("Path is not a file - {f}".format(f=loc))
                raise IsADirectoryError(errno.EISDIR, os.strerror(errno.EISDIR), loc)
            else:
                return fnc(*args, **kwargs)
        return cast(F, wrap)
    if _fnc is None:
        return dec
    else:
        return dec(_fnc)


def is_dir(_fnc: Optional[F] = None, *, var_name: str, pos: int = 0) -> Any:
    """ Throw a NotADirectoryError if location given as an argument is not a directory

    Uses exists function to first confirm that path exists
    :param var_name: Name of argument containing path
    :param pos: Position in argument list of argument containing path (0 index)
    """
    def dec(fnc: F) -> F:
        @functools.wraps(fnc)
        @exists(var_name=var_name, pos=pos)
        def wrap(*args, **kwargs) -> Any:
            loc = kwargs[var_name] if var_name in kwargs else args[pos]
            logging.debug("Confirming that path is a directory - {f}".format(f=loc))
            if not os.path.isdir(loc):
                logging.error("Path is not a directory - {f}".format(f=loc))
                raise NotADirectoryError(errno.ENOTDIR, os.strerror(errno.ENOTDIR), loc)
            else:
                return fnc(*args, **kwargs)
        return cast(F, wrap)
    if _fnc is None:
        return dec
    else:
        return dec(_fnc)


def make_dirs(_fnc: Optional[F] = None, *, var_name: str, pos: int = 0) -> Any:
    """ Create required folders if dirname of path returned doesn't exist

    :param var_name: Name of argument containing path
    :param pos: Position in argument list of argument containing path (0 index)
    """
    def dec(fnc: F) -> F:
        @functools.wraps(fnc)
        def wrap(*args, **kwargs) -> Any:
            out = fnc(*args, **kwargs)
            out_dir = os.path.dirname(out)
            if not os.path.exists(out_dir):
                os.makedirs(out_dir)
            return out
        return cast(F, wrap)
    if _fnc is None:
        return dec
    else:
        return dec(_fnc)
