""" Decorator functions relating to logging

"""


# Imports
import functools
import logging
from typing import Callable, TypeVar, cast, Any, Optional


F = TypeVar('F', bound=Callable[..., Any])


class DisableLogger:
    def __init__(self, lvl):
        "Initialise DisableLogger class"
        self.level = lvl

    def __enter__(self):
        "Disable Logging for defined level"
        logging.disable(self.level)

    def __exit__(self, type, vl, tb):
        "Enable logging again"
        logging.disable(logging.NOTSET)

    @property
    def level(self):
        "Return Logging level to disable"
        return self._lvl

    @level.setter
    def level(self, lvl):
        "Set level to disable"
        self._lvl = lvl


def no_logging_level(_fnc: Optional[F] = None, *, level) -> Any:
    """ Temporarily pause logging for the given level and below

    :param lvl: Logging level to disable
    """
    def dec(fnc: F) -> F:
        @functools.wraps(fnc)
        def wrap(*args, **kwargs) -> Any:
            with DisableLogger(level):
                out = fnc(*args, **kwargs)
            return out
        return cast(F, wrap)
    if _fnc is None:
        return dec
    else:
        return dec(_fnc)


def no_logging_all(fnc: F) -> F:
    "Temporarily pause all logging"
    @functools.wraps(fnc)
    def wrap(*args, **kwargs) -> Any:
        return no_logging_level(_fnc=fnc, level=logging.CRITICAL)(*args, **kwargs)
    return cast(F, wrap)


def no_logging_info(fnc: F) -> F:
    "Temporarily pause logging at INFO level and below"
    @functools.wraps(fnc)
    def wrap(*args, **kwargs) -> Any:
        return no_logging_level(fnc, level=logging.INFO)(*args, **kwargs)
    return cast(F, wrap)


def no_logging_debug(fnc: F) -> F:
    "Temporarily pause logging at DEBUG level and below"
    @functools.wraps(fnc)
    def wrap(*args, **kwargs) -> Any:
        return no_logging_level(fnc, level=logging.DEBUG)(*args, **kwargs)
    return cast(F, wrap)
