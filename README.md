# STL Decorators

Custom decorator functions.

### Files

Decorators to assess whether paths exist and whether they are files or directories.

#### Input

Decorators to assess whether the paths given as function arguments exist, are files, or are directories.

To use these decorators, the path argument to be assessed must be identified by argument name and position (0 index).

##### files.input.exists

Confirm that a path given as a function argument exists. Raise a `FileNotFoundError` if the path does not exist.

```python
@stldecorators.files.input.exists(var_name="path", pos=0)
def my_fnc(path):
    ...  # Your code
```

##### files.input.is_file

Confirm that a path given as a function argument exists, and is a file. Calls the `exists` function first to assess whether the path exists. Raises a `IsADirectoryError` if the path is a directory instead of a file.

```python
@stldecorators.files.input.is_file(var_name="path", pos=0)
def my_fnc(path):
    ...  # Your code
```

##### files.input.is_dir

Confirm that a path given as a function argument exists, and is a directory. Calls the `exists` function first to assess whether the path exists. Raises a `NotADirectoryError` if the path is a file instead of a directory.

```python
@stldecorators.file.input.is_dir(var_name="path", pos=0)
def my_fnc(path):
    ...  # Your code
```

##### files.input.make_dirs

For a path given as a function argument, if the directory does not exist, the required folders will be created. Assesses `os.path.dirname()`, so only the required folders will be created, not any files.

```python
@stldecorators.file.input.make_dirs(var_name="path", pos=0)
def my_fnc(path):
    ...  # Your code
```

#### Output

Decorators to assess whether the path given as an output of a function exist, is a file, or is a directory.

##### files.output.exists

Confirm that the path returned from a function exists. Raise a `FileNotFoundError` if the path does not exist.

```python
@stldecorators.files.output.exists
def fnc():
    ... # Your code
    return path
```

##### files.output.is_file

Confirm that the path returned from a function exists and is a file. Calls the `exists` function first to assess whether the path exists. Raises a `IsADirectoryError` if the path is a directory instead of a file.

```python
@stldecorators.files.output.is_file
def my_fnc():
    ...  # Your code
    return path
```

##### files.output.is_dir
Confirm that the path returned from a function exists and is a directory. Calls the `exists` function first to assess whether the path exists. Raises a `NotADirectoryError` if the path is a file instead of a directory.

```python
@stldecorators.files.output.is_dir
def my_fnc():
    ...  # Your code
    return path
```

##### files.output.make_dirs

For a path returned from a function, if the directory does not exist, the required folders will be created. Assesses `os.path.dirname()`, so only the required folders will be created, not any files.

```python
@stldecorators.file.output.make_dirs
def my_fnc():
    ...  # Your code
    return path
```

### Logging

Decorators to temporarily disable logging at a given level.

##### logging.no_logging_level

Temporarily disable logging for a given logging level and below, regardless of the configured logging level.

```python
@stldecorators.logging.no_logging_level(level=logging.INFO)
def my_fnc():
    ...
    logging.warning("Warning log")  # will show, assuming logging is configured to show warnings
    logging.info("Info log")  # Will not show
    logging.debug("Debug log")  # Will not show
    ...
```

##### logging.no_logging_all

Temporarily disable logging for all logging levels regardless of the configured logging level (assuming there are no custom logging levels added above `logging.CRITICAL`).

```python
@stldecorators.logging.no_logging_all
def my_fnc():
    ...
    logging.critical("Critical log")  # Will not show
    logging.debug("Debug log")  # Will not show
    ...
```

##### logging.no_logging_info

Temporarily disable logging for `logging.INFO` and below, regardless of the configured logging level.

```python
@stldecorators.logging.no_logging_info
def my_fnc():
    ...
    logging.warning("Warning log")  # Will show
    logging.info("Info log")  # Will not show
    logging.debug("Debug log")  # Will not show
    ...
```

##### logging.no_logging_debug

Temporarily disable logging for `logging.DEBUG` and below, regardless of the configured logging level.

```python
@stldecorators.logging.no_logging_debug
def my_fnc():
    ...
    logging.warning("Warning log")  # Will show
    logging.info("Info log")  # Will show
    logging.debug("Debug log")  # Will not show
    ...
```
