""" Unit Testing

"""


# Imports
import os
import sys
import unittest

fldr = os.path.normpath(os.path.join(os.path.dirname(__file__), ".."))
if fldr not in sys.path:
    sys.path.append(fldr)

import stldecorators  # noqa:E402


@stldecorators.logging.no_logging_all
@stldecorators.files.input.exists(var_name="loc", pos=0)
def _input_exists(loc: str) -> str:
    "Return location"
    return loc


@stldecorators.logging.no_logging_all
@stldecorators.files.input.is_file(var_name="loc", pos=0)
def _input_is_file(loc: str) -> str:
    "Return location"
    return loc


@stldecorators.logging.no_logging_all
@stldecorators.files.input.is_dir(var_name="loc", pos=0)
def _input_is_dir(loc: str) -> str:
    "Return location"
    return loc


@stldecorators.logging.no_logging_all
@stldecorators.files.output.exists
def _output_exists(loc: str) -> str:
    "Return location"
    return loc


@stldecorators.logging.no_logging_all
@stldecorators.files.output.is_file
def _output_is_file(loc: str) -> str:
    "Return location"
    return loc


@stldecorators.logging.no_logging_all
@stldecorators.files.output.is_dir
def _output_is_dir(loc: str) -> str:
    "Return location"
    return loc


class TestFileDecorators(unittest.TestCase):
    def setUp(self) -> None:
        "Initialise class"
        self._file_existing = os.path.abspath(__file__)
        self._file_not_existing = os.path.abspath(os.path.join(os.path.dirname(__file__), "doesntexist", "madeupfile.ext"))
        self._dir_existing = os.path.abspath(os.path.dirname(__file__))
        self._dir_not_existing = os.path.abspath(os.path.join(os.path.dirname(__file__), "does", "not", "exist"))

    def test_input_exists_pass(self) -> None:
        "Confirm no action by decorator when input path exists"
        self.assertEqual(_input_exists(self._dir_existing), self._dir_existing)

    def test_input_exists_fail(self) -> None:
        "Confirm FileNotFoundError when input path does not exist"
        self.assertRaises(FileNotFoundError, _input_exists, self._dir_not_existing)

    def test_input_is_file_pass(self) -> None:
        "Confirm no action by decorator when input file path is an existing file"
        self.assertEqual(_input_is_file(self._file_existing), self._file_existing)

    def test_input_is_file_fail_exists(self) -> None:
        "Confirm FileNotFoundError when input file path does not exist"
        self.assertRaises(FileNotFoundError, _input_is_file, self._file_not_existing)

    def test_input_is_file_fail_file(self) -> None:
        "Confirm IsADirectoryError when input file path is an existing directory instead of a file"
        self.assertRaises(IsADirectoryError, _input_is_file, self._dir_existing)

    def test_input_is_dir_pass(self) -> None:
        "Confirm no action by decorator when input directory path is an existing directory"
        self.assertEqual(_input_is_dir(self._dir_existing), self._dir_existing)

    def test_input_is_dir_fail_exists(self) -> None:
        "Confirm FileNotFoundError when input directory path does not exist"
        self.assertRaises(FileNotFoundError, _input_is_dir, self._dir_not_existing)

    def test_input_is_dir_fail_dir(self) -> None:
        "Confirm NotADirectoryError when input directory path is an existing file not a directory"
        self.assertRaises(NotADirectoryError, _input_is_dir, self._file_existing)

    def test_output_exists_pass(self) -> None:
        "Confirm no action by decorator when output path exists"
        self.assertEqual(_output_exists(self._dir_existing), self._dir_existing)

    def test_output_exists_fail(self) -> None:
        "Confirm FileNotFoundError when output path does not exist"
        self.assertRaises(FileNotFoundError, _output_exists, self._dir_not_existing)

    def test_output_is_file_pass(self) -> None:
        "Confirm no action by decorator when output file path is an existing file"
        self.assertEqual(_output_is_file(self._file_existing), self._file_existing)

    def test_output_is_file_fail_exists(self) -> None:
        "Confirm FileNotFoundError when output file path does not exist"
        self.assertRaises(FileNotFoundError, _output_is_file, self._file_not_existing)

    def test_output_is_file_fail_file(self) -> None:
        "Confirm IsADirectoryError when output file path is an existing directory instead of a file"
        self.assertRaises(IsADirectoryError, _output_is_file, self._dir_existing)

    def test_output_is_dir_pass(self) -> None:
        "Confirm no action by decorator when output directory path is an existing directory"
        self.assertEqual(_output_is_dir(self._dir_existing), self._dir_existing)

    def test_output_is_dir_fail_exists(self) -> None:
        "Confirm FileNotFoundError when output directory path does not exist"
        self.assertRaises(FileNotFoundError, _output_is_dir, self._dir_not_existing)

    def test_output_is_dir_fail_file(self) -> None:
        "Confirm NotADirectoryError when output directory path is an existing file not a directory"
        self.assertRaises(NotADirectoryError, _output_is_dir, self._file_existing)


if __name__ == "__main__":
    unittest.main()
