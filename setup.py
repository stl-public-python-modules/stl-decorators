""" Setup Script

"""

import setuptools

with open("README.md") as fl:
    desc = fl.read()

setuptools.setup(
    name="stldecorators"
    , version="0.0.1"
    , author="Stuart Lambert"
    , author_email="stuart.t.lambert@gmail.com"
    , description="Custom decorator functions"
    , long_description=desc
    , long_Description_content_type="text/markdown"
    , url="https://gitlab.com/python-modules1/stl-decorators"
    , packages=setuptools.find_packages()
    , classifiers=[
        "Programming Language :: Python :: 3"
        , "License :: OSI Approved :: MIT License"
        , "Operating System :: OS Independent"
        ]
    , python_requires='>=3.6'
)
